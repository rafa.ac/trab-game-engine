﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class Rocket : MonoBehaviour
{
    public Canvas canvas;
    public Image fuelBar;

    public float rotationSpeed = 170f;
    public float thrustSpeed = 25f;

    public AudioSource audioSourcePickups;

    public AudioClip soundEngine;
    public AudioClip soundFinish;
    public AudioClip soundDeath;
    public AudioClip soundLife;
    public AudioClip soundFuel;

    public ParticleSystem particleEngine;
    public ParticleSystem particleSuccess;
    public ParticleSystem particleDeath;

    private Rigidbody rigidbody;
    private AudioSource audioSource;
    private LevelControl.LevelControl levelControl;

    private float fuel;

    private const string TAG_FRIENDLY = "Friendly";
    private const string TAG_FINISH = "Finish";
    private const string TAG_LIFE = "Life";
    private const string TAG_FUEL = "Fuel";

    GameObject[] hearts;
    GameObject[] fuelCans;

    private PlayerStats playerHealth;

    enum State { Alive, Dying, Transcending}
    State state = State.Alive;

    // Start is called before the first frame update
    void Start()
    {
        canvas.enabled = false;
        levelControl = LevelControl.LevelControl.Instance;
        rigidbody = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();
        hearts = GameObject.FindGameObjectsWithTag(TAG_LIFE);
        fuelCans = GameObject.FindGameObjectsWithTag(TAG_FUEL);

        FillFuel();

        playerHealth = PlayerStats.Instance;
    }

    // Update is called once per frame
    void Update()
    {
        ChangeLevel();

        if (this.state == State.Alive)
        {
            ProcessInput();
        }
    }

    private void ProcessInput()
    {
        Thrust();
        Rotate();
    }

    void ChangeLevel()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            movePlayerToLevel(1);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            movePlayerToLevel(2);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            movePlayerToLevel(3);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            movePlayerToLevel(4);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            movePlayerToLevel(5);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha6))
        {
            movePlayerToLevel(6);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (state == State.Alive)
        {
            if (other.tag == TAG_LIFE)
            {
                audioSourcePickups.PlayOneShot(soundLife);
                playerHealth.Heal(1f);
            }
            else if (other.tag == TAG_FUEL)
            {
                audioSourcePickups.PlayOneShot(soundFuel);
                FillFuel();
            }

            other.gameObject.SetActive(false);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (this.state != State.Alive)
            return;

        switch (collision.gameObject.tag)
        {
            case TAG_FRIENDLY:
                break;
            case TAG_FINISH:
                audioSource.Stop();
                audioSource.PlayOneShot(soundFinish);
                particleSuccess.Play();
                state = State.Transcending;
                canvas.enabled = true;
                
                break;
            default:
                state = State.Dying;
                audioSource.Stop();
                audioSource.PlayOneShot(soundDeath);
                particleDeath.Play();
                playerHealth.TakeDamage(1f);

                if (playerHealth.Health >= 1)
                {
                    Invoke("LoadScene", 1f);
                }
                else
                {
                    canvas.enabled = true;
                    Text t = canvas.GetComponentInChildren<Text>();
                    t.text = "Fim de jogo!";
                    t.color = UnityEngine.Color.white;
                }

                break;
        }
    }

    private void Thrust()
    {
        fuelBar.fillAmount = fuel;

        if (Input.GetKey(KeyCode.Space))
        {
            fuel -= 0.003f;
            rigidbody.useGravity = true;

            if (fuel > 0)
            {
                rigidbody.AddRelativeForce(Vector3.up * thrustSpeed);

                // checks if audio isnt already playing
                if (!audioSource.isPlaying)
                {
                    audioSource.PlayOneShot(soundEngine);
                }
                particleEngine.Play();
            }
            else
            {
                audioSource.Stop();
                particleEngine.Stop();
            }
        }
        else
        {
            // stops engine sound
            audioSource.Stop();

            // stops engine particles
            particleEngine.Stop();
        }
    }

    private void Rotate()
    {
        float rotation = this.rotationSpeed * Time.deltaTime;
        rigidbody.freezeRotation = true;

        if (Input.GetKey(KeyCode.A))
        {
            transform.Rotate(Vector3.forward * rotation);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            transform.Rotate(Vector3.back * rotation);
        }

        rigidbody.freezeRotation = false;
    }

    private void LoadScene()
    {
        particleDeath.Stop();

        state = State.Alive;
        FillFuel();
        ResetFuelPickups();

        movePlayerToLevel(levelControl.Level);
    }

    void movePlayerToLevel(int level)
    {
        Vector3 position = new Vector3();

        switch (level)
        {
            case 1:
                position = new Vector3(-58f, 2f, 0);
                break;
            case 2:
                position = new Vector3(50f, 80f, 0);
                break;
            case 3:
                position = new Vector3(48f, 170f, 0);
                break;
            case 4:
                position = new Vector3(48f, 250f, 0);
                break;
            case 5:
                position = new Vector3(44f, 320f, 0);
                break;
            case 6:
                position = new Vector3(-55f, 412f, 0);
                break;
        }

        transform.position = position;
        transform.rotation = new Quaternion();
        rigidbody.velocity = Vector3.zero;
        rigidbody.angularVelocity = Vector3.zero;
        rigidbody.useGravity = false;
    }

    private void ResetHeartPickups()
    {
        foreach (var heart in hearts)
        {
            heart.SetActive(true);
        }
    }

    private void ResetFuelPickups()
    {
        foreach (var fuelCan in fuelCans)
        {
            fuelCan.SetActive(true);
        }
    }

    private void FillFuel()
    {
        fuel = 1f;
    }

    public void ResetGame()
    {
        print("ResetGame");
        particleDeath.Stop();
        ResetHeartPickups();
        ResetFuelPickups();

        state = State.Alive;
        levelControl.Level = 1;
        playerHealth.Heal(5f);

        FillFuel();

        canvas.enabled = false;
        LoadScene();
    }

    public void QuitGame()
    {
        print("sair");
        Application.Quit();
    }
}
