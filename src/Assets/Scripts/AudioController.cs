﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using LevelControl;

public class AudioController : MonoBehaviour
{
    public AudioSource audioSource;

    public AudioClip underground;
    public AudioClip outside;
    public AudioClip mainMenu;
    public AudioClip gameOver;

    private PlayerStats playerHealth;

    private LevelControl.LevelControl levelControl;

    // Start is called before the first frame update
    void Start()
    {
        playerHealth = PlayerStats.Instance;
        levelControl = LevelControl.LevelControl.Instance;

        audioSource = GetComponent<AudioSource>();

        audioSource.clip = underground;
        audioSource.loop = true;
        audioSource.Play();
    }

    // Update is called once per frame
    void Update()
    {
        if (SceneManager.GetActiveScene().buildIndex == 0)
        {
            setAudioClip(mainMenu);
        }
        else
        {
            if (playerHealth.Health >= 1)
            {
                if (levelControl.Level == 6)
                {
                    setAudioClip(outside);
                }
                else
                {
                    setAudioClip(underground);
                }
            }
            else
            {
                setAudioClip(gameOver);
            }
        }
        
    }

    private void setAudioClip(AudioClip clip)
    {
        if (audioSource.clip != clip)
        {
            audioSource.clip = clip;
            audioSource.loop = true;
            audioSource.Play();
        }
    }
}
