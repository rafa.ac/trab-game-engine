﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    private Vector3 startPosition;
    private bool isMovingUp = true;

    [SerializeField]
    Vector3 movementSpeed;

    [SerializeField]
    Vector3 maxDistanceToMove;

    [Range(0, 1)]
    [SerializeField]
    float movement;

    // Start is called before the first frame update
    void Start()
    {
        startPosition = transform.position;   
    }

    // Update is called once per frame
    void Update()
    {
        float f = (startPosition + maxDistanceToMove).y;

        if (isMovingUp)
            transform.position += (movementSpeed * Time.deltaTime);
        else
            transform.position -= (movementSpeed * Time.deltaTime);

        if (transform.position.y > f)
            isMovingUp = false;
        else if (transform.position.y <= startPosition.y)
            isMovingUp = true;
    }
}
