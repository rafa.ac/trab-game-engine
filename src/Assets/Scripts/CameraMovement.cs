﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    public GameObject player;
    private Vector3 initialPosition;
    private float camPosY;
    private LevelControl.LevelControl levelControl;
    bool moveCamera = true;
    Vector3 finalPosition;
     
    // Start is called before the first frame update
    void Start()
    {
        //finalPosition = new Vector3(transform.position.x, 120f, transform.position.z);
        finalPosition = transform.position;
        initialPosition = transform.position;
        camPosY = initialPosition.y;

        levelControl = LevelControl.LevelControl.Instance;
        levelControl.Level = 1;
    }

    // Update is called once per frame
    void Update()
    {
        float playerPosY = player.transform.position.y;

        if (playerPosY < 55f)
        {
            camPosY = initialPosition.y;
            finalPosition = new Vector3(transform.position.x, camPosY, transform.position.z);
            transform.position = Vector3.Lerp(transform.position, finalPosition, 2 * Time.deltaTime);
            levelControl.Level = 1;
        }
        else if (playerPosY >= 55f && playerPosY < 144f)
        {
            camPosY = 117f;
            finalPosition = new Vector3(transform.position.x, camPosY, transform.position.z);
            transform.position = Vector3.Lerp(transform.position, finalPosition, 2 * Time.deltaTime);
            levelControl.Level = 2;
        }
        else if (playerPosY >= 144f && playerPosY < 230f)
        {
            camPosY = 207f;
            finalPosition = new Vector3(transform.position.x, camPosY, transform.position.z);
            transform.position = Vector3.Lerp(transform.position, finalPosition, 2 * Time.deltaTime);
            levelControl.Level = 3;
        }
        else if (playerPosY >= 230f && playerPosY < 310f)
        {
            camPosY = 285f;
            finalPosition = new Vector3(transform.position.x, camPosY, transform.position.z);
            transform.position = Vector3.Lerp(transform.position, finalPosition, 2 * Time.deltaTime);
            levelControl.Level = 4;
        }
        else if (playerPosY >= 310f && playerPosY < 392f)
        {
            camPosY = 360f;
            finalPosition = new Vector3(transform.position.x, camPosY, transform.position.z);
            transform.position = Vector3.Lerp(transform.position, finalPosition, 2 * Time.deltaTime);
            levelControl.Level = 5;
        }
        else if (playerPosY >= 392f)
        {
            camPosY = 454f;
            finalPosition = new Vector3(transform.position.x, camPosY, transform.position.z);
            transform.position = Vector3.Lerp(transform.position, finalPosition, 2 * Time.deltaTime);
            levelControl.Level = 6;
        }
    }
}
